package com.davidgiordana.sgroupedlist

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.davidgiordana.sgroupedlist.data.SGLModel
import com.davidgiordana.sgroupedlist.data.cells.SGLCellData
import com.davidgiordana.sgroupedlist.data.cells.SGLSectionTitleCellData
import com.davidgiordana.sgroupedlist.data.cells.SGLTitleDetailCellData
import com.davidgiordana.sgroupedlist.viewholders.SGLSectionTitleViewHolder
import com.davidgiordana.sgroupedlist.viewholders.SGLTitleDetailCellViewHolder
import com.davidgiordana.sgroupedlist.viewholders.SGLViewHolder
import kotlin.reflect.KClass

typealias ViewHolderCreator = (ViewGroup) -> SGLViewHolder

open class SGLListAdapter(private val model: SGLModel) : RecyclerView.Adapter<SGLViewHolder>(), SGLModel.Delegate {

    private data class CellRegister(val id: String, val viewHolderCreator: ViewHolderCreator)

    // MARK: - Data

    private val TAG = "SGLListAdapter"

    private val cellRegisters = ArrayList<CellRegister>()

    // MARK: - Init

    init {
        this.model.delegate = this

        this.registerCell(SGLSectionTitleCellData::class) { parent ->
            val view = SGLSectionTitleCellData.inflateFrom(parent)
            SGLSectionTitleViewHolder(view)
        }

        this.registerCell(SGLTitleDetailCellData::class) { parent ->
            val view = SGLTitleDetailCellData.inflateFrom(parent)
            SGLTitleDetailCellViewHolder(view)
        }
    }

    // MARK: - Interface

    fun <T: SGLCellData> registerCell(cellDataType: KClass<T>, viewHolderCreator: ViewHolderCreator) {
        val register = CellRegister(cellDataType.simpleName!!, viewHolderCreator)
        this.cellRegisters.add(register)
    }

    // MARK: - Adapter

    override fun getItemViewType(position: Int): Int {
        val cellData = model[position]
        val cellType = cellData::class.simpleName
        val registerIndex = this.cellRegisters.indexOfFirst { it.id == cellType }

        if (registerIndex == -1) {
            throw NoSuchElementException("Cell $cellType is not registered")
        }

        return registerIndex
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SGLViewHolder {
        val cellType = cellRegisters[viewType].id
        val cellRegister = this.cellRegisters.first{ it.id == cellType }

        val viewHolder = cellRegister.viewHolderCreator(parent)
        viewHolder.itemView.setOnClickListener { processAction(viewHolder) }
        return viewHolder
    }

    override fun getItemCount(): Int {
        return model.count
    }

    override fun onBindViewHolder(holder: SGLViewHolder, position: Int) {
        holder.setData(model[position])
    }

    // MARK: - Internal

    private fun processAction(viewHolder: SGLViewHolder) {
        val cell = viewHolder.cellDataData
        this.model.onAction(cell)
    }

    // MARK: - SGLModel.Delegate

    override fun sglmodelDidInsert(insertions: SGLModelInsertion) {
        val indexes = insertions.items.sortedDescending()
        for (index in indexes) {
            this.notifyItemInserted(index)
        }
    }

    override fun sglmodelDidDelete(deletions: SGLModelDeletion) {
        val indexes = deletions.items.sortedDescending()
        for (index in indexes) {
            this.notifyItemRemoved(index)
        }
    }

    override fun sglmodelDidUpdate(deletions: SGLModelUpdate) {
        val indexes = deletions.items.sortedDescending()
        for (index in indexes) {
            this.notifyItemChanged(index)
        }
    }

}