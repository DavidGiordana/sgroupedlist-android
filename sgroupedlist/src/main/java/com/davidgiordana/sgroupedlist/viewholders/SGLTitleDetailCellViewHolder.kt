package com.davidgiordana.sgroupedlist.viewholders

import android.view.View
import android.widget.TextView
import com.davidgiordana.sgroupedlist.R
import com.davidgiordana.sgroupedlist.data.cells.SGLCellData
import com.davidgiordana.sgroupedlist.data.cells.SGLTitleDetailCellData

class SGLTitleDetailCellViewHolder(itemView: View): SGLViewHolder(itemView) {

    override fun setData(cellDataData: SGLCellData) {
        super.setData(cellDataData)

        if (cellDataData !is SGLTitleDetailCellData) {
            return
        }

        val titleTextView = itemView.findViewById<TextView>(R.id.title_text_view)
        titleTextView.text = cellDataData.title

        val detailTextView = itemView.findViewById<TextView>(R.id.detail_text_view)
        detailTextView.text = cellDataData.detail
    }

}