package com.davidgiordana.sgroupedlist.viewholders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.davidgiordana.sgroupedlist.data.cells.SGLCellData

open class SGLViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    lateinit var cellDataData: SGLCellData

    open fun setData(cellDataData: SGLCellData) {
        this.cellDataData = cellDataData
    }

}