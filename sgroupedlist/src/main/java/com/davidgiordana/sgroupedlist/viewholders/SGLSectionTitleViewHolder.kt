package com.davidgiordana.sgroupedlist.viewholders

import android.view.View
import android.widget.TextView
import com.davidgiordana.sgroupedlist.R
import com.davidgiordana.sgroupedlist.data.SGLSectionData
import com.davidgiordana.sgroupedlist.data.cells.SGLCellData

class SGLSectionTitleViewHolder(itemView: View): SGLViewHolder(itemView) {

    override fun setData(cellDataData: SGLCellData) {
        super.setData(cellDataData)

        if (cellDataData !is SGLSectionData) {
            return
        }

        val titleTextView = itemView.findViewById<TextView>(R.id.title_text_view)
        titleTextView.text = cellDataData.title
    }

}