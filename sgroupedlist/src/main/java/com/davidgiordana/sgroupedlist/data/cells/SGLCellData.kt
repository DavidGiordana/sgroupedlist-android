package com.davidgiordana.sgroupedlist.data.cells

interface SGLCellData {

    val id: String?

    var title: String

    var actionID: String?

}