package com.davidgiordana.sgroupedlist.data.cells

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.davidgiordana.sgroupedlist.R

class SGLTitleDetailCellData(override var title: String = "",
                             var detail: String? = null,
                             override val id: String? = null,
                             override var actionID: String? = null
) : SGLCellData {

    companion object {

        fun inflateFrom(parent: ViewGroup): View {
            val inflater = LayoutInflater.from(parent.context)
            return inflater.inflate(R.layout.cell_title_detail, parent, false)
        }

    }

}