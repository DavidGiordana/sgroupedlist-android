package com.davidgiordana.sgroupedlist.data

import com.davidgiordana.sgroupedlist.data.cells.SGLCellData

class SGLSectionData(
    override val id: String,
    override var title: String,
    override var actionID: String?
) : SGLCellData