@file:Suppress("RedundantVisibilityModifier", "unused")

package com.davidgiordana.sgroupedlist.data

import com.davidgiordana.sgroupedlist.SGLModelDeletion
import com.davidgiordana.sgroupedlist.SGLModelInsertion
import com.davidgiordana.sgroupedlist.SGLModelUpdate
import com.davidgiordana.sgroupedlist.data.cells.SGLCellData
import com.davidgiordana.sgroupedlist.data.cells.SGLSectionTitleCellData

typealias SGLCellAction = (SGLModel, SGLCellData) -> Unit

class SGLModel (
    private val items: MutableList<SGLCellData> = ArrayList(),
    private val actionsByID: MutableMap<String, SGLCellAction> = HashMap(),
    private val actionsByCellID: MutableMap<String, SGLCellAction> = HashMap(),
    private val actionsByType: MutableMap<String, SGLCellAction> = HashMap()
) {

    interface Delegate {
        fun sglmodelDidInsert(insertions: SGLModelInsertion)
        fun sglmodelDidDelete(deletions: SGLModelDeletion)
        fun sglmodelDidUpdate(deletions: SGLModelUpdate)
    }

    // MARK: - Getters

    val count: Int
        get() = this.items.size

    operator fun get(index: Int): SGLCellData {
        return items[index]
    }

    var delegate: Delegate? = null

    // MARK: Add

    public fun addSection(id: String, title: String) {
        val section = SGLSectionTitleCellData(id, title)

        this.items.add(section)

        val insertions = SGLModelInsertion(listOf(items.size - 1))
        this.delegate?.sglmodelDidInsert(insertions)
    }

    public fun addCell(cell: SGLCellData) {
        this.items.add(cell)

        val insertions = SGLModelInsertion(listOf(items.size - 1))
        this.delegate?.sglmodelDidInsert(insertions)
    }

    public fun addCellatBeginingOfSection(sectionID: String, cell: SGLCellData) {
        val sectionIdx = getSectionCellIndex(sectionID) ?: return

        val itemIndex = sectionIdx + 1
        this.items.add(itemIndex, cell)

        val insertions = SGLModelInsertion(listOf(itemIndex))
        this.delegate?.sglmodelDidInsert(insertions)
    }

    public fun addCellatEndOfSection(sectionID: String, cell: SGLCellData) {
        val sectionIndex = getSectionCellIndex(sectionID) ?: return

        // Search for end of section index
        var indexToInsert = sectionIndex + 1

        for (index in indexToInsert until items.size) {
            if (items[index] is SGLSectionTitleCellData) {
                break
            }
            indexToInsert += 1
        }

        this.items.add(indexToInsert, cell)

        val insertions = SGLModelInsertion(listOf(indexToInsert))
        this.delegate?.sglmodelDidInsert(insertions)
    }

    // MARK:  Remove

    public fun removeCellbyIndex(cellIndex: Int) {
        if (this.items.isEmpty()) {
            return
        }

        this.items.removeAt(cellIndex)

        val deletions = SGLModelDeletion(listOf(cellIndex))
        this.delegate?.sglmodelDidDelete(deletions)
    }

    public fun removeCellbyId(cellID: String) {
        var indexToDelete: Int? = null

        for ((index, item) in items.withIndex()) {
            if (item.id == cellID) {
                indexToDelete = index
                break
            }
        }

        if (indexToDelete != null) {
            this.removeCellbyIndex(indexToDelete)
        }
    }

    // MARK:  Action

    public fun addActionByActionID(actionID: String, action: SGLCellAction) {
        this.actionsByID[actionID] = action
    }

    public fun addActionByCellID(cellID: String, action: SGLCellAction) {
        this.actionsByCellID[cellID] = action
    }

    public fun <T: SGLCellData> addActionByType(clazz: Class<T>, action: SGLCellAction) {
        val type = clazz.simpleName
        this.actionsByType[type] = action
    }

    public fun onAction(cell: SGLCellData) {
        onActionByID(cell) || onActionByCellID(cell) || onActionByType(cell)
    }

    private fun onActionByID(cell: SGLCellData): Boolean {
        val actionID = cell.actionID ?: return false
        val action = this.actionsByID[actionID] ?: return false

        action(this, cell)

        return true
    }

    private fun onActionByCellID(cell: SGLCellData): Boolean {
        val cellID = cell.id ?: return false
        val action = actionsByCellID[cellID] ?: return false

        action(this, cell)

        return true
    }

    private fun onActionByType(cell: SGLCellData): Boolean {
        val cellType = cell.javaClass.simpleName
        val action = this.actionsByType[cellType] ?: return false

        action(this, cell)

        return true
    }

    private fun getSectionCellIndex(sectionID: String): Int? {
        for ((index, item) in items.withIndex()) {
            if (item is SGLSectionTitleCellData && item.id == sectionID) {
                return index
            }
        }

        return null
    }

}