package com.davidgiordana.sgroupedlist

data class SGLModelInsertion(val items: List<Int>)

data class SGLModelDeletion(val items: List<Int>)

data class SGLModelUpdate(val items: List<Int>)